# README #

### What is this repository for? ###

* Lego Mindstorms Line Follower Robot with NXT C code

### How do I get set up? ###

* Get a Mindstorms robot
* Install modified NXT/Lego firmware
* Be able to make and flash .rxe files to the LEGO brick (using cygwin, nxtosek, GNUARM)
* Make the physical robot ( Two rear servos for wheels, one servo controlling steering thru steering rack )
* Calibrate the MED_LIGHT define in HT2.c to the middle ground between black/white light sensor tests
* Follow lines all night